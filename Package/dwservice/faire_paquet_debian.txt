# Outil à installer :
# sudo apt-get install debhelper cdbs lintian build-essential fakeroot devscripts pbuilder dh-make debootstrap
#
# https://www.debian-fr.org/t/extraire-paquet-deb/31870
#
# Pour extraire le paquet :
#
# dpkg-deb -x paquet.deb repertoire -> extrait l'arborescence
# dpkg-deb -e paquet.deb -> extrait le répertoire DEBIAN contenant les différents fichiers postinst, control, etc
#
# Pour assembler le paquet :
#
# dpkg-deb -b repertoire paquet.deb
#

# Méthode paquet origine
nom=dwservice
version=1.0.5

# Méthode assemblage

dpkg-deb -b ${nom} ${nom}_${version}_all.deb


