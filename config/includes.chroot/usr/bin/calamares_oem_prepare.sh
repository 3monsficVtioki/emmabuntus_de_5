#! /bin/bash

# calamares_oem_prepare.sh --
#
#   This file permits to prepare OEM postinstall for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

bureau=${XDG_DESKTOP_DIR}

if [[ ($USER == "oem") ]] ; then

    bureau=${XDG_DESKTOP_DIR}
    file_desktop=${bureau}/install-debian.desktop

    # Préparation pour le lancement de la OEM Postinstall
    # Création du fichier desktop sur le bureau

    if [[ -f ${file_desktop} ]] ; then
        rm ${file_desktop}
    fi

    cp -f /opt/calamares/install-debian-oem.desktop ${file_desktop}
    chmod a+x ${file_desktop}

    # Pour autoriser le lancement de Calamares sur le bureau pour LXQt
    gio set ${file_desktop} -t string metadata::trust "true"

    # Pour autoriser le lancement de Calamares sur le bureau pour Xfce et LXQt
    gio set -t string ${file_desktop} metadata::xfce-exe-checksum "$(sha256sum ${file_desktop} | awk '{print $1}')"

    # Patch hostname
    if [[ $(cat /etc/hostname | grep localhost.localdomain) ]] ; then

        echo "oem-pc" | sudo tee /etc/hostname

        sudo tee /etc/hosts > /dev/null <<EOT
127.0.0.1   localhost
127.0.0.1   oem-pc.unassigned-domain oem-pc

# The following lines are desirable for IPv6 capable hosts
::1         localhost ip6-localhost ip6-loopback
ff02::1     ip6-allnodes
ff02::2     ip6-allrouters
EOT

    fi

    # Patch temps d'arrêt
    conf_file=/etc/systemd/system.conf
    if [[ $(cat ${conf_file} | grep "#DefaultTimeoutStopSec=90s") ]] ; then

        sed s/^#DefaultTimeoutStopSec=90s/DefaultTimeoutStopSec=15s/ ${conf_file} | sudo tee ${conf_file}.tmp
        sudo chmod a+r ${conf_file}.tmp
        sed s/^#DefaultTimeoutStartSec=90s/DefaultTimeoutStartSec=15s/ ${conf_file}.tmp | sudo tee ${conf_file}
        sudo rm ${conf_file}.tmp

    fi

    exit 0

else

    exit 1

fi



